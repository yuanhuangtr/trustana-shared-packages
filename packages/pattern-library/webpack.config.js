const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: "production",
  entry: {
    index: "./index.ts",
    i18n: "./i18n.ts",
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.module\.s(a|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: true,
              sourceMap: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.svg$/i,
        issuer: /\.[jt]sx?$/,
        use: [
          {
            loader: "@svgr/webpack",
            options: {
              icon: true,
              dimensions: false,
              replaceAttrValues: { color: "currentColor" },
            },
          },
        ],
      },
    ],
  },
  externals: {
    react: 'react',
    "react-dom":"react-dom",
  },
  resolve: {
    extensions: [".tsx", ".ts", ".scss"],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
  experiments: {
    outputModule: true,
  },
  // the code below is the setting to umd
  // output: {
  //   filename: "[name].js",
  //   path: __dirname + "/.dist",
  //   library: "@trustana/pattern-library",
  //   // libraryTarget: "umd",
  //   umdNamedDefine: true,
  //   globalObject: "this",
  // },
  output: {
    filename: "[name].js",
    path: __dirname + "/.dist",
    clean: true,
    publicPath: "/",
    libraryTarget: "module",
  },
};
