import XIcon from "@trustana/icons/outline/x.svg";
import clsx from "clsx";
import React, { useEffect } from "react";
import ClientOnlyPortal from "./ClientOnlyPortal";
import type { ModalFooterProps } from "./ModalFooter";
import ModalFooter from "./ModalFooter";
import styles from "./modal.module.scss";
import { BaseProps } from "../../interface/BaseProps";
import { Text, TextEnum } from "../text";
import { isServer } from "../../utils/environments";

export enum ModalSize {
  SMALL = "SMALL",
  MEDIUM = "MEDIUM",
  LARGE = "LARGE",
}

export interface ModalProps extends ModalFooterProps, BaseProps {
  size?: ModalSize;
  title?: string | null;
  open?: boolean;
  onClose?: () => void;
  children?: React.ReactNode;
}

export const Modal: React.FC<ModalProps> = ({
  title,
  open,
  onClose,
  size = ModalSize.MEDIUM,
  children,
  className,
  onCancel,
  onConfirm,
  cancelLabel,
  confirmLabel,
}: ModalProps) => {
  const ModalSizeMapping = {
    [ModalSize.SMALL]: styles.small,
    [ModalSize.MEDIUM]: styles.medium,
    [ModalSize.LARGE]: styles.large,
  };

  const sizeClass = ModalSizeMapping[size];

  const hideFooter = !onCancel && !onConfirm && !cancelLabel && !confirmLabel;

  const handleClose = () => {
    if (onClose) onClose();
  };

  const handleClickModal = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
  };

  useEffect(() => {
    if (!open || isServer()) return;

    document.body.style.overflow = "hidden";

    return () => {
      document.body.style.overflow = "auto";
    };
  }, [open]);

  return (
    <>
      {open && (
        <ClientOnlyPortal containerId="modal">
          <div className={styles.backdrop} onClick={handleClose}>
            <div
              className={clsx(styles.panel, sizeClass, className)}
              onClick={handleClickModal}
            >
              <div className={styles.header}>
                <XIcon className={styles.closeIcon} onClick={handleClose} />
                {title && <Text type={TextEnum.Subhead_big}>{title}</Text>}
              </div>
              <div className={styles.content}>{children}</div>
              <ModalFooter
                onCancel={onCancel}
                onConfirm={onConfirm}
                cancelLabel={cancelLabel}
                confirmLabel={confirmLabel}
                hidden={hideFooter}
              />
            </div>
          </div>
        </ClientOnlyPortal>
      )}
    </>
  );
};
