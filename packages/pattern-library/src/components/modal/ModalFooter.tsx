import React from "react";
import { Button } from "../index";
import styles from "./modal.module.scss";
import withHidden, { HiddenPros } from "../../hoc/withHidden";

export interface ModalFooterProps extends HiddenPros {
  onCancel?: () => void;
  onConfirm?: () => void;
  cancelLabel?: string | null;
  confirmLabel?: string | null;
}

const ModalFooter: React.ComponentType<ModalFooterProps> = withHidden(
  ({ onCancel, onConfirm, cancelLabel, confirmLabel }) => {
    const handleCancel = () => {
      if (onCancel) onCancel();
    };

    const handleConfirm = () => {
      if (onConfirm) onConfirm();
    };

    return (
      <div className={styles.footer}>
        {cancelLabel && onCancel && (
          <Button type="secondary" onClick={handleCancel}>
            {cancelLabel}
          </Button>
        )}
        {confirmLabel && onConfirm && (
          <Button onClick={handleConfirm}>{confirmLabel}</Button>
        )}
      </div>
    );
  }
);

export default ModalFooter;
