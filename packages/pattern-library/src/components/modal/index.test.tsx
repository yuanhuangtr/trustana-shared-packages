import { cleanup, fireEvent, render, screen } from "@testing-library/react";
import { Modal } from "./Modal";
import { faker } from "@faker-js/faker";
import userEvent from "@testing-library/user-event";

describe("Modal Component", () => {
  beforeEach(cleanup);
  test("modal could be opened", async () => {
    const mockTitle = faker.random.words();
    const mockContent = faker.random.words();
    const mockConfirmTxt = faker.random.words();
    const mockCancelTxt = faker.random.words();
    const mockClose = jest.fn();
    const mockConfirm = jest.fn();
    const mockCancel = jest.fn();

    const { baseElement } = render(
      <Modal
        title={mockTitle}
        open={true}
        onClose={mockClose}
        onConfirm={mockConfirm}
        onCancel={mockCancel}
        confirmLabel={mockConfirmTxt}
        cancelLabel={mockCancelTxt}
      >
        {mockContent}
      </Modal>
    );
    const backDrop = baseElement.getElementsByClassName("backdrop")[0];
    expect(backDrop).not.toBeNull();
    expect(await screen.findByText(mockTitle)).not.toBeNull();
    expect(await screen.findByText(mockContent)).not.toBeNull();
    // close icon
    const xIconEle = baseElement.getElementsByClassName("closeIcon")[0];
    expect(xIconEle).not.toBeNull();
    await userEvent.click(xIconEle);
    expect(mockClose).toBeCalled();
    // confirm ele
    const confirmEle = await screen.findByText(mockConfirmTxt);
    expect(confirmEle).not.toBeNull();
    await userEvent.click(confirmEle);
    expect(mockConfirm).toBeCalled();
    // cancel ele
    const cancelEle = await screen.findByText(mockCancelTxt);
    expect(cancelEle).not.toBeNull();
    await userEvent.click(cancelEle);
    expect(mockCancel).toBeCalled();
  });
});
