import React from "react";
import { createPortal } from "react-dom";
import { isServer } from "../../utils/environments";

interface ClientOnlyPortalProps {
  children: React.ReactNode;
  containerId?: string;
}

const ClientOnlyPortal: React.FC<ClientOnlyPortalProps> = ({
  children,
  containerId,
}: ClientOnlyPortalProps) => {
  if (isServer()) return <></>;

  let container: null | HTMLElement = document.body;
  if (containerId) {
    let target = document.getElementById(containerId);
    if (!target) {
      const newDiv = document.createElement("div");
      newDiv.id = containerId;
      document.body.appendChild(newDiv);
      target = document.getElementById(containerId);
    }
    container = target;
  }

  return <> {createPortal(children, container as Element)}</>;
};

export default ClientOnlyPortal;
