import React, { ReactElement, ReactNode } from "react";
import clsx from "clsx";
import { BaseProps } from "../../interface/BaseProps";
import style from "./button.module.scss";

export type ButtonType = "default" | "secondary" | "filled" | "textLink";

export type ButtonSize = "default" | "small";

export interface ButtonProps extends BaseProps {
  type?: ButtonType;
  size?: ButtonSize;
  disabled?: boolean;
  onClick: () => void;
  children: ReactNode;
}

const sizeMapping = {
  ["default"]: style.btn_size_default,
  ["small"]: style.btn_size_small,
};

const textLinkMapping = {
  ["default"]: style.btn_size_default_textLink,
  ["small"]: style.btn_size_small_textLink,
}

const typeMapping = {
  ["default"]: style.btn_default,
  ["secondary"]: style.btn_secondary,
  ["filled"]: style.btn_filled,
  ["textLink"]: style.btn_textLink,
}

const getStyle = (type: ButtonType, size: ButtonSize) => {

  if(type === "textLink") {
    return [style.btn_textLink, textLinkMapping[size]];
  }

  return [typeMapping[type], sizeMapping[size]];
};

export const Button: React.FC<ButtonProps> = ({
  children,
  onClick,
  type = "default",
  size = "default",
  className,
  disabled,
}: ButtonProps): ReactElement<ButtonProps> => {
  const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    e.stopPropagation();
    onClick();
  };

  return (
    <button
      type="button"
      className={clsx(getStyle(type, size), className)}
      disabled={disabled}
      onClick={handleClick}
    >
      {children}
    </button>
  );
};
