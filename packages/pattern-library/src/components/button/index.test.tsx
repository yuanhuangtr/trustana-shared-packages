import { cleanup, render, screen } from "@testing-library/react";
import { Button } from "./.";
import { faker } from "@faker-js/faker";
import userEvent from "@testing-library/user-event";

describe("Button Component", () => {
  beforeEach(cleanup);
  test("button could be clicked", async () => {
    const mockText = faker.random.word();
    const mockClick = jest.fn();
    render(<Button onClick={mockClick}>{mockText}</Button>);
    const ele = await screen.findByText(mockText);
    expect(ele).toHaveTextContent(mockText);
    await userEvent.click(ele);
    expect(mockClick).toBeCalled();
    expect(ele).toHaveClass('btn_size_default');
    expect(ele).toHaveClass('btn_default');
  });

  test("button contains correct style", async () => {
    const mockText = faker.random.word();
    const mockClick = jest.fn();
    render(
      <Button onClick={mockClick} type="textLink" size="small">
        {mockText}
      </Button>
    );
    const ele = await screen.findByText(mockText);

    expect(ele).toHaveClass('btn_size_small_textLink');
    expect(ele).toHaveClass('btn_textLink');
  });
});
