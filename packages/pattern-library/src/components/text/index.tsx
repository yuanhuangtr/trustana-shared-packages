import React from "react";
import clsx from "clsx";
import { BaseProps } from "../../interface/BaseProps";
import style from "./text.module.scss";

export enum TextEnum {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  Subhead_big,
  Subhead_medium,
  Subhead_small,
  Body_big,
  Body_medium,
  Body_small,
  Caption,
  Button_large,
  Button_small,
  Button_nav,
}

export interface TextProps extends BaseProps {
  children: string | React.ReactNode;
  onClick?: () => void;
  type?: TextEnum;
}

type TagMappingType = {
  [value in TextEnum]: string;
};

type ClassNameType = {
  [value in TextEnum]: string;
};

const typeMapping: TagMappingType = {
  [TextEnum.H1]: "h1",
  [TextEnum.H2]: "h2",
  [TextEnum.H3]: "h3",
  [TextEnum.H4]: "div",
  [TextEnum.H5]: "div",
  [TextEnum.H6]: "div",
  [TextEnum.Subhead_big]: "div",
  [TextEnum.Subhead_medium]: "div",
  [TextEnum.Subhead_small]: "div",
  [TextEnum.Body_big]: "div",
  [TextEnum.Body_medium]: "div",
  [TextEnum.Body_small]: "div",
  [TextEnum.Caption]: "div",
  [TextEnum.Button_large]: "span",
  [TextEnum.Button_small]: "span",
  [TextEnum.Button_nav]: "span",
};

const classNameMapping: ClassNameType = {
  [TextEnum.H1]: style.h1,
  [TextEnum.H2]: style.h2,
  [TextEnum.H3]: style.h3,
  [TextEnum.H4]: style.h4,
  [TextEnum.H5]: style.h5,
  [TextEnum.H6]: style.h6,
  [TextEnum.Subhead_big]: style.subhead_big,
  [TextEnum.Subhead_medium]: style.subhead_medium,
  [TextEnum.Subhead_small]: style.subhead_small,
  [TextEnum.Body_big]: style.body_big,
  [TextEnum.Body_medium]: style.body_medium,
  [TextEnum.Body_small]: style.body_small,
  [TextEnum.Caption]: style.caption,
  [TextEnum.Button_large]: style.button_large,
  [TextEnum.Button_small]: style.button_small,
  [TextEnum.Button_nav]: style.button_nav,
};

export const Text: React.FC<TextProps> = ({
  type = TextEnum.H1,
  className,
  ...rest
}: TextProps) => {
  const tagType = typeMapping[type];
  const classes = clsx(classNameMapping[type], className);

  return React.createElement(tagType, { className: classes, ...rest });
};
