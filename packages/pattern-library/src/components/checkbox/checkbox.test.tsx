import { cleanup, render, screen } from "@testing-library/react";
import { Checkbox } from "./index";
import { faker } from "@faker-js/faker";
import userEvent from "@testing-library/user-event";


describe("Checkbox Component", () => {
  beforeEach(cleanup);
  test("Checkbox could be clicked", async () => {
    const mockClick = jest.fn();
    const mockLabel = faker.random.words()
    const {container } = render(<Checkbox onClick={mockClick} label={mockLabel} checked />);
    const ele = await screen.findByText(mockLabel);
    expect(ele).toHaveTextContent(mockLabel);
    await userEvent.click(ele);
    expect(mockClick).toBeCalledWith(false);
    expect(container.firstElementChild).toHaveClass("checkbox_wrapper");
  });

  test("Checkbox in indeterminate click state and no label state", async () => {
    const mockClick = jest.fn();
    const {container} = render(<Checkbox onClick={mockClick} label={null} indeterminate />);
    const root  = container.firstElementChild;
    expect(root).toHaveClass("checkbox_indeterminate");
    await userEvent.click(root);
    expect(mockClick).toBeCalledWith(true);
    expect(root.getElementsByTagName("div").length).toBe(0);
  })
})
