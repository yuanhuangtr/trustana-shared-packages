import React, { useEffect, useState } from "react";
import clsx from "clsx";
import { BaseProps } from "../../interface/BaseProps";
import { Text, TextEnum } from "../text";
import style from "./checkbox.module.scss";

export interface CheckboxProps extends BaseProps {
  checked?: boolean;
  label?: string | null;
  indeterminate?: boolean;
  onClick: (val: boolean) => void;
  disabled?: boolean;
}

export const Checkbox: React.FC<CheckboxProps> = ({
  label,
  checked = false,
  indeterminate = false,
  onClick,
  className,
  disabled = false,
}: CheckboxProps) => {
  // must use state, to refresh component
  const [val, setVal] = useState(checked);

  const isIndeterminate = indeterminate && !val;

  const handleClick = (e: React.MouseEvent | React.ChangeEvent) => {
    e.preventDefault();
    e.stopPropagation();
    onClick(!checked);
  };

  useEffect(() => {
    setVal(checked);
  }, [checked]);

  if (!label || label.length === 0) {
    return (
      <input
        type="checkbox"
        className={clsx(
          style.checkbox,
          { [style.checkbox_indeterminate]: isIndeterminate },
          className
        )}
        onChange={handleClick}
        checked={val}
        disabled={disabled}
      />
    );
  }

  return (
    <label
      className={clsx(
        style.checkbox_wrapper,
        disabled && style.checkbox_wrapper_disabled,
        className
      )}
      onClick={handleClick}
    >
      <input
        type="checkbox"
        className={clsx(style.checkbox, {
          [style.checkbox_indeterminate]: isIndeterminate,
        })}
        onChange={handleClick}
        checked={val}
        disabled={disabled}
      />
      <Text
        type={TextEnum.Body_medium}
        className={clsx(style.label, disabled && style.label_disabled)}
      >
        {label}
      </Text>
    </label>
  );
};
