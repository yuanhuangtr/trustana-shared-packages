import type { ReactElement } from "react";
import React from "react";

export interface HiddenPros {
  hidden?: boolean;
}

const withHidden = function withHidden<P extends object>(
  Component: React.ComponentType<P>,
): React.ComponentType<P> {
  function withHiddenComponent(props: P & HiddenPros): ReactElement<P> {
    const { hidden } = props;
    if (hidden) {
      return <></>;
    }

    return <Component {...(props as P)} />;
  }

  return withHiddenComponent;
};

export default withHidden;
