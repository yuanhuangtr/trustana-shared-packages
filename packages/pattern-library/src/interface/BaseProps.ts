
// to keep the component shared props use the same name
export interface BaseProps {
  className?: string | string[];
}
