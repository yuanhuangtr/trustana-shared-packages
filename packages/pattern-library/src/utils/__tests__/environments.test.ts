import { isBrowser, isServer } from "../environments";

describe("environment", () => {
  let tempWindow: Window & typeof globalThis;
  beforeEach(() => {
    tempWindow = { ...window };
  });
  afterEach(() => {
    window = tempWindow;
  });
  test("isBrowser worked", () => {
    const windowSpy = jest
      // @ts-ignore
      .spyOn(global, "window", "get")
      .mockReturnValueOnce(undefined)
      .mockReturnValueOnce({});
    expect(isBrowser()).toBeFalsy();
    expect(windowSpy).toBeCalled();
    expect(isBrowser()).toBeTruthy();
    expect(windowSpy).toBeCalled();
  });

  test("isServer worked", () => {
    const windowSpy = jest
      // @ts-ignore
      .spyOn(global, "window", "get")
      .mockReturnValueOnce(undefined)
      .mockReturnValueOnce({});
    expect(isServer()).toBeTruthy();
    expect(windowSpy).toBeCalled();
    expect(isBrowser()).toBeFalsy();
    expect(windowSpy).toBeCalled();
  });
});
