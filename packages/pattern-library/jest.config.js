
module.exports = {
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: [
    "/node_modules",
    "/public",
    "/.yarn",
    "/.husky",
    "/.vscode",
    "/cicd",
    "/styles"
  ],
  testRegex: [
    "/tests/.*\\.(test|spec)?\\.(ts|tsx)$",
    "/src/.*\\.(test|spec)?\\.(ts|tsx)$",
  ],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  setupFilesAfterEnv: ["<rootDir>/tests/setup/setupTests.ts"],
  modulePaths: ["<rootDir>/src"],
  collectCoverageFrom: ["!**/node_modules/**", "<rootDir>/src/**/*.{ts,tsx}"],
  "moduleNameMapper": {
    "\\.(css|less|scss|sss|styl)$": "<rootDir>/../../node_modules/jest-css-modules",
    '\\.svg$': '<rootDir>/tests/__mocks__/svg.ts',
  }
};
