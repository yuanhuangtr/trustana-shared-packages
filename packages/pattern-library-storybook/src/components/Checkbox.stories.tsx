import { Checkbox, Text, TextEnum } from "@trustana/pattern-library";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import React, { useState } from "react";

export default {
  title: "Components/Checkbox",
  component: Checkbox,
  argTypes: {
    checked: { control: "boolean" },
    label: { control: "text", name: "checkbox label" },
    indeterminate: { control: "boolean" },
    disabled: { control: "boolean" },
    onClick: { action: "clicked" },
  },
} as ComponentMeta<typeof Checkbox>;

const DefaultTemplate: ComponentStory<typeof Checkbox> = ({
  checked,
  label,
  disabled,
  indeterminate,
  onClick,
}) => {
  return (
    <Checkbox
      onClick={onClick}
      checked={checked}
      indeterminate={indeterminate}
      disabled={disabled}
      label={label}
    />
  );
};

export const Component = DefaultTemplate.bind({});

Component.args = {
  disabled: false,
  checked: false,
  label: "Checkbox label",
  indeterminate: false,
};

export const Documents = () => {
  const handleClick = (val: boolean) => {
    console.log(val);
  };
  return (
    <div>
      <Text type={TextEnum.H1}>checkbox component:</Text>
      <Text type={TextEnum.H6}>checkbox unchecked:</Text>
      <Checkbox onClick={handleClick} label="checkbox label" />
      <Text type={TextEnum.H6}>checkbox checked:</Text>
      <Checkbox onClick={handleClick} label="checkbox label" checked />
      <Text type={TextEnum.H6}>
        checkbox unchecked with indeterminate status:
      </Text>
      <Checkbox onClick={handleClick} label="checkbox label" indeterminate />

      <Text type={TextEnum.H4}>when checkbox disabled</Text>
      <Text type={TextEnum.H6}>checkbox unchecked:</Text>
      <Checkbox onClick={handleClick} label="checkbox label" disabled />
      <Text type={TextEnum.H6}>checkbox checked:</Text>
      <Checkbox onClick={handleClick} label="checkbox label" checked disabled />
      <Text type={TextEnum.H6}>
        checkbox unchecked with indeterminate status:
      </Text>
      <Checkbox
        onClick={handleClick}
        label="checkbox label"
        indeterminate
        disabled
      />

      <Text type={TextEnum.H4}>when checkbox with no label value:</Text>
      <div style={{ display: "flex", gap: "8px" }}>
        <Checkbox onClick={handleClick} checked />
        <Checkbox onClick={handleClick} />
        <Checkbox onClick={handleClick} indeterminate />
        <Checkbox onClick={handleClick} checked disabled />
        <Checkbox onClick={handleClick} disabled />
        <Checkbox onClick={handleClick} indeterminate disabled />
      </div>
    </div>
  );
};

export const CheckboxExample: ComponentStory<typeof Checkbox> = ({indeterminate}) => {
  const [checked, setChecked] = useState(false);

  const handleClick = () => {
    setChecked(!checked);
  };

  return (
    <>
      <Checkbox onClick={handleClick} checked={checked} label="checkbox label" indeterminate={indeterminate} />
    </>
  );
};
