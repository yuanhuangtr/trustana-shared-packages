import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Button } from "@trustana/pattern-library";
import { Text, TextEnum } from "@trustana/pattern-library";
import React from "react";

export default {
  title: "Components/Button",
  component: Button,
  argTypes: {
    children: { control: "text", name: "button label" },
    type: {
      control: "select",
      options: ["default", "secondary", "filled", "textLink"],
      defaultValue: "default",
      name: "button type",
    },
    size: {
      control: "select",
      options: ["default", "small"],
      defaultValue: "default",
      name: "button size",
    },
    disabled: { control: "boolean" },
    onClick: { action: "clicked" },
  },
} as ComponentMeta<typeof Button>;

const DefaultTemplate: ComponentStory<typeof Button> = ({
  children,
  disabled,
  type,
  size,
  onClick,
}) => {
  return (
    <Button onClick={onClick} disabled={disabled} type={type} size={size}>
      {children}
    </Button>
  );
};

export const Component = DefaultTemplate.bind({});

Component.args = {
  children: "some text",
  disabled: false,
  type: "default",
  size: "default",
};

export const Default = () => {
  const fn = () => console.log("click");
  return (
    <div>
      <Text type={TextEnum.H1}>Default</Text>
      <Button onClick={fn}>Default</Button>
      <Text type={TextEnum.H1}>Small</Text>
      <Button onClick={fn} size="small">
        Small
      </Button>
      <Text type={TextEnum.H1}>Default Disabled</Text>
      <Button onClick={fn} disabled>
        Default
      </Button>
    </div>
  );
};

export const Secondary = () => {
  const fn = () => console.log("click");
  return (
    <div>
      <Text type={TextEnum.H1}>Default</Text>
      <Button onClick={fn} type="secondary">
        Default
      </Button>
      <Text type={TextEnum.H1}>Small</Text>
      <Button onClick={fn} type="secondary" size="small">
        Small
      </Button>
      <Text type={TextEnum.H1}>Default Disabled</Text>
      <Button onClick={fn} type="secondary" disabled>
        Default
      </Button>
    </div>
  );
};

export const Filled = () => {
  const fn = () => console.log("click");
  return (
    <div>
      <Text type={TextEnum.H1}>Default</Text>
      <Button onClick={fn} type="filled">
        Default
      </Button>
      <Text type={TextEnum.H1}>Small</Text>
      <Button onClick={fn} type="filled" size="small">
        Small
      </Button>
      <Text type={TextEnum.H1}>Default Disabled</Text>
      <Button onClick={fn} type="filled" disabled>
        Default
      </Button>
    </div>
  );
};

export const TextLink = () => {
  const fn = () => console.log("click");
  return (
    <div>
      <Text type={TextEnum.H1}>Default</Text>
      <Button onClick={fn} type="textLink">
        Default
      </Button>
      <Text type={TextEnum.H1}>Small</Text>
      <Button onClick={fn} type="textLink" size="small">
        Small
      </Button>
      <Text type={TextEnum.H1}>Default Disabled</Text>
      <Button onClick={fn} type="textLink" disabled>
        Default
      </Button>
    </div>
  );
};
