import { Modal, Button } from "@trustana/pattern-library";
import { useState } from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import {
  ModalProps,
  ModalSize,
} from "@trustana/pattern-library";

export default {
  title: "Components/Modal",
  component: Modal,
  argTypes: {
    title: { control: "text", name: "modal title" },
    children: { control: "text", name: "modal content" },
    open: { control: "boolean" },
    onClose: { action: "onClose" },
    cancelLabel: { control: "text", name: "cancelLabel" },
    confirmLabel: { control: "text", name: "confirmLabel" },
    onCancel: { action: "onCancel" },
    onConfirm: { action: "onConfirm" },
  },
} as ComponentMeta<typeof Modal>;

const DefaultTemplate: ComponentStory<typeof Modal> = ({
  title,
  children,
  size,
  open,
  onClose,
  onCancel,
  onConfirm,
  cancelLabel,
  confirmLabel,
}: ModalProps) => {
  return (
    <Modal
      title={title}
      size={size}
      open={open}
      onClose={onClose}
      onCancel={onCancel}
      onConfirm={onConfirm}
      cancelLabel={cancelLabel}
      confirmLabel={confirmLabel}
    >
      {children}
    </Modal>
  );
};

export const Component = DefaultTemplate.bind({});

Component.args = {
  title: "modal title",
  children: "content ",
  cancelLabel: " cancel",
  confirmLabel: " confirm",
  open: false,
  size: ModalSize.MEDIUM,
};

export const defaultModal = () => {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleClick}>open modal</Button>
      <Modal
        title="mock title"
        open={open}
        onClose={handleClose}
        confirmLabel="confirm"
        cancelLabel="cancel"
        onConfirm={handleClose}
        onCancel={handleClose}
      >
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br /> content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br /> content <br />
        content <br />
        content <br />
        content <br />
        content <br />
        content <br />
      </Modal>
    </div>
  );
};
