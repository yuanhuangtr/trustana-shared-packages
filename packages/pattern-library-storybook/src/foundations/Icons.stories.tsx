import Mail from "@trustana/icons/outline/mail.svg";
import React from "react";

export default {
  title: "foundations/Icons",
};

export const Outline = () => (
  <div>
    Outline icons
    <div>
      <Mail />
    </div>
  </div>
);

export const Solid = () => <div>Solid icons</div>;

export const Graphic = () => <div>graphic</div>;
