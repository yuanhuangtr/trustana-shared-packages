import { Text, TextEnum } from "@trustana/pattern-library";
import React from "react";

export default {
  title: "foundations/Typography",
  component: Text,
};

export const Header = () => {
  return (
    <div>
      <div className="mgt-8">
        <Text type={TextEnum.H1}>H1</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.H2}>H2</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.H3}>H3</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.H4}>H4</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.H5}>H5</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.H6}>H6</Text>
      </div>
    </div>
  );
};

export const Subheader = () => {
  return (
    <div>
      <div className="mgt-8">
        <Text type={TextEnum.Subhead_big}>Subhead_big</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.Subhead_medium}>Subhead_medium</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.Subhead_small}>Subhead_small</Text>
      </div>
    </div>
  );
};

export const Body = () => {
  return (
    <div>
      <div className="mgt-8">
        <Text type={TextEnum.Body_big}>Body_big</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.Body_medium}>Body_medium</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.Body_small}>Body_small</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.Caption}>Caption</Text>
      </div>
    </div>
  );
};

export const Button = () => {
  return (
    <div>
      <div className="mgt-8">
        <Text type={TextEnum.Button_large}>Button_large</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.Button_small}>Button_small</Text>
      </div>
      <div className="mgt-8">
        <Text type={TextEnum.Button_nav}>Button_nav</Text>
      </div>
    </div>
  );
};
