// add styles for pattern-library
// import "@trustana/pattern-library/styles/_all.scss";
import "@trustana/pattern-library/index.css";


export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
    expanded: true
  },
}
