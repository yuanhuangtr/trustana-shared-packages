
# trustana shared packages

## setup environment

### yarn 3

<https://next.yarnpkg.com/getting-started/install>

maker sure your yarn version is 3.3.1

## commands in this project

### yarn workspace

add package into packages in workspace

```shell
yarn workspace [sub-project] add --save [packageName]

```
example:

```shell
yarn workspace @trustana/pattern-library add react
```


### storybook

start storybook

```shell
yarn storybook
```
### run script

```shell
yarn nx build @trustana/pattern-library
```


### publish packages

1. login to aws:


```shell
yarn login:aws
```


2. auto update version

```shell
yarn version
# update 1 small version example 0.0.1->0.0.2
# or

yarn version:minor
# update 1 minor version example: 0.1.3 -> 0.2.1
```

3. publish to aws

```shell
yarn publish:all
```
